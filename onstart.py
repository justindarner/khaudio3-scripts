#!/usr/bin/python
from subprocess import call
import os

scriptsPath = os.path.expanduser("~/scripts")
os.chdir(scriptsPath)
call(["git", "pull"])
call(["python", "main.py"])
