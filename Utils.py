#!/usr/bin/python
import datetime
import os
import errno
import json
import urllib2
import base64

now = datetime.datetime.now()

username = "#parkerjw#"
password = "#parkerjw#"


def downloadPicture(url, folder,name):
  res = urllib2.urlopen(url)
  type = res.info()['Content-Type']
  if type.startswith("image"):
    print "Downloading Picture " + url
    ext = type.replace('image/', '')
    file = open( folder + name + "." + ext, "w")
    file.write(res.read())
    file.close()

def isTodaySunday():
  return now.strftime( "%w") == "0"

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def rm(path):
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception, e:
            print e

def getLastMonday():
  today = datetime.date.today()
  monday = today - datetime.timedelta(days=today.weekday())
  return monday.isoformat()


def fetchCurrentMeeting():
  type = "study";
  if isTodaySunday():
      type = "watchtower"

  week_starting = getLastMonday() + "T06:00:00.000Z"
  url = 'https://meetings.darner.org/api/meetings/?type=' + type + '&week_starting=' + week_starting
  request = urllib2.Request(url)
  base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
  request.add_header("Authorization", "Basic %s" % base64string)
  try:
    data = json.load(urllib2.urlopen(request))
    print "Songs: " + str(data['songs'])
    return data
  except Exception,e:
    print e
