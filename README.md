# HOW TO

1. Go to applications and run terminal 
2. type: ‘mkdir scripts; cd scripts’
3. type: ‘git clone https://bitbucket.org/justindarner/khaudio3-scripts.git .’
4. Go to System Preferences / Users and Groups / Login items
5. Press + and select the file ‘onstart.py’ found in the scripts folder created.
6. Make sure the onstart script is checked marked.


# Requirements
1. Mac with KHAudio3 installed
2. You must have git installed on a Mac.  

When you type git in terminal, it should install.

You may need to run: xcode-select --install

