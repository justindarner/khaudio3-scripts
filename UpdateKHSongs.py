#!/usr/bin/python
import sys
import os.path
from xml.etree import ElementTree as et
dataFile = "/Library/Application Support/KHAudio3/UserPreferences.xml"

def updateSongPreference(song1, song2, song3):
  if not os.path.isfile(dataFile) :
    print "Could not find file '" + dataFile + "'"
    exit(1)

  tree = et.parse(dataFile)
  tree.find('song1').text = str(song1)
  tree.find('song2').text = str(song2)
  tree.find('song3').text = str(song3)
  tree.write(dataFile)
  print "Updated KHAudio3 Song list -> " + dataFile

if __name__ == "__main__":
  if( len(sys.argv) != 3+1):
      print "Please enter three song numbers!"
      exit()

  updateSongPreference(sys.argv[1], sys.argv[2], sys.argv[3])



