import Utils
import os
watchtowerFolder = os.path.expanduser("~/Desktop/AutoSlides/watchtower/")
studyFolder      = os.path.expanduser("~/Desktop/AutoSlides/midweek/")

def download(pics):
    folder = studyFolder
    if Utils.isTodaySunday():
        folder = watchtowerFolder

    Utils.mkdir_p(folder)
    Utils.rm(folder)

    for pic in pics:
        Utils.downloadPicture(pic['url'], folder, pic['name'])
