import os
os.environ['PYTHONDONTWRITEBYTECODE']="1"
from subprocess import call
import Utils
import Pictures
import UpdateKHSongs

applescript =  'display dialog "'
applescript += 'Press OK to fetch Songs and Pictures.\n'
applescript += '** Please do not open KHAudio3 until complete.'
applescript += '" with title "Songs and Pictures"'

answer = os.popen("osascript -e '{}'".format(applescript)).read()
if not "ok" in answer.lower():
    exit(0)

try:
  meeting = Utils.fetchCurrentMeeting()
  songs = meeting['songs']
  UpdateKHSongs.updateSongPreference(songs[0], songs[1], songs[2])

  pics =  meeting['pictures']
  picTitle = ""
  if Utils.isTodaySunday():
      picTitle = "Watchtower Study"
  else:
      picTitle = "Bible Study"

  Pictures.download(pics)

  print songs
  print pics

  applescript =  'display dialog "'
  applescript += 'Songs:  [' + ', '.join(str(x) for x in songs) + '] were set.\n'
  applescript += 'Total Pictures found for ' + picTitle + ' : ' + str(len(pics)) + '.'
  applescript += '" with title "Songs and Slides" buttons {"OK"}'

  call("osascript -e '{}'".format(applescript), shell=True)
except Exception,e:
  print e
  applescript =  'display dialog "'
  applescript += 'There was an error fetching the Songs and Pictures. Sorry...'
  applescript += '" with title "Songs and Slides" buttons {"OK"}'

  call("osascript -e '{}'".format(applescript), shell=True)


